#include <string>
#include <random>

using namespace std;

string randDNA(int s, string b, int n)
{
	mt19937 Rand_Eng(s);

	int min = 0;
	int max = (b.size() - 1);

	uniform_int_distribution<> uniform(min, max);
	string ATCG;
	for(int i = 1; i <= n; i++)
	{
		ATCG += b[uniform(Rand_Eng)];
	}
	return ATCG;
}
